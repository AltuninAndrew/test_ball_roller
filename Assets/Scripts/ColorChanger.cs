﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ColorChanger : MonoBehaviour
{

    public float colorChangeSpeed = 0.8f;
    private bool _isUpdate = false;
    private float _startTime;
    private Material _material;

    public CubesController controller { get; set; }
    void Start()
    {

        _material = GetComponent<Renderer>().material;
    }

    void Update()
    {
        if (_isUpdate && _material.color != Color.black)
        {
            float t = Time.time - _startTime;
            _material.color = Color.Lerp(Color.white, Color.black, t * colorChangeSpeed);
            if (_material.color == Color.black)
            {
                controller.NotifChange();
            }
        }

    }

    private void OnCollisionEnter(Collision collision)
    {
        if (collision.collider.tag == "Player" && _isUpdate == false)
        {
            _startTime = Time.time;
            _isUpdate = true;

        }

        
    }
    

}
