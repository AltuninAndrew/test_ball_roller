﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class CubesController : MonoBehaviour
{
    
    public int maxNumCubeOnScene = 0;
    public int minNumCubeOnScene = 0;
    public int NumCubeOnScene { get; private set; } = 0;

    [SerializeField]
    private GameObject _prefab = null;
    [SerializeField]
    private Transform[] bordersX = new Transform[2]; //crutch x1. Fix it
    [SerializeField]
    private Transform[] bordersZ = new Transform[2]; //crutch x2. Fix it

    [SerializeField]
    private UIController uIController = null;

    private Vector3 _randomPos = new Vector3();
    private int _numCubesChanged = 0;

    void Start()
    {

        NumCubeOnScene = Random.Range(minNumCubeOnScene, maxNumCubeOnScene);
        uIController.SetTotalAmmount(NumCubeOnScene);
        uIController.SetNumActiveValue(_numCubesChanged);
        GenerateNewMap();
    }

    
    void GenerateNewMap()
    {
        for(int i=0;i<NumCubeOnScene;i++)
        {
            float randomX = Random.Range(bordersX[0].position.x, bordersX[1].position.x);
            float randomZ = Random.Range(bordersZ[0].position.z, bordersZ[1].position.z);
           _randomPos.Set(randomX,bordersX[0].position.y,randomZ);
            GameObject newCube = Instantiate(_prefab, _randomPos, Quaternion.identity);
            newCube.GetComponent<ColorChanger>().controller = this;
            newCube.name = "Cube " + i;
        }
        
    }

    public void NotifChange()
    {
        _numCubesChanged++;
        uIController.SetNumActiveValue(_numCubesChanged);
        if(_numCubesChanged == NumCubeOnScene)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().name,LoadSceneMode.Single);
        }
    }


}
