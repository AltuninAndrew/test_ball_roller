﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SphereController : MonoBehaviour
{
    [SerializeField]
    private float _speed = 200;

    private Rigidbody _body;
    private Vector3 _movementVector;

    void Start()
    {
        _body = GetComponent<Rigidbody>();
        _movementVector = new Vector3();
    }

    
    void Update()
    {
        
        if(Input.GetMouseButton(0) || Input.GetMouseButton(1))
        {
            _movementVector.Set(-Input.GetAxis("Mouse Y") * Time.deltaTime * _speed, 0, Input.GetAxis("Mouse X") * Time.deltaTime * _speed);
            _body.velocity = _movementVector;
        }

    }
}
