﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIController : MonoBehaviour
{

    [SerializeField]
    private Text _txtAmmountCube = null;

    private string _text;
    private int _numActive;
    private int _totalAmmount;

    private void UpdateUI()
    {
        _text = _numActive + " / " + _totalAmmount;
        _txtAmmountCube.text = _text;
    }

    public void SetNumActiveValue(int numActive)
    {
        _numActive = numActive;
        UpdateUI();
    }

    public void SetTotalAmmount(int totalAmmount)
    {
        _totalAmmount = totalAmmount;
    }



}
